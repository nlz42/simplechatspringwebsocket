package main.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import main.hello.ChatMessage;

@Controller
public class ChatController {
	
	@MessageMapping("/hello") //recieved message from
	@SendTo("/topic/greetings") // the response is send as broadcast channel to /topic/greetings
	public ChatMessage greeting (ChatMessage message){
		System.out.println(message.getName());
		System.out.println(message.getContent());
		return message;
	}

}
