var stompClient = null;
var name = "unknown";

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect(){
    var socket = new SockJS('/chat-app');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (antwort) {
        	printMessage('<b>'+JSON.parse(antwort.body).name +'</b><BLOCKQUOTE>'+JSON.parse(antwort.body).content);
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    stompClient.send("/app/hello", {}, JSON.stringify({'name': name, 'content':$("#message").val()}));
}

function setName(){
	name = $("#meinName").val();
	document.getElementById('nameField').innerHTML = 'Dein Name: '+name;
	$("#setNameField").hide();
}

function printMessage(message) {
    $("#greetings").append("<tr><td>"+ message + "</td></tr>");
}

$(function () {
	connect();
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendMessage(); });
    $ ("#setName").click(function() { setName(); });
});