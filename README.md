# About #

This is a real simple Chat Program. It is just to show how a websocket Communication is worked and how to implement it.
Please note that there is no security or else, you can just send messages over a website.

### Built with: ###

* Maven Project
* Java Programming Language
* Spring Framework

Generale purpose: A little look how to implement a Websocket connection with Java and Spring Framework.
The example application is a Chat-Program - so best way is to use two Browser in private-mode to see communication without reload pages.

### Getting Started ###

It is a maven Java Project. Feel free to clone it and run it.

### How do I get set up? ###

* Clone Repo
* Build it with maven
* start the *.jar file in the target folder
* see how it works

### Code is docmentation enough###

This application has a focus to keep it simple, so just read the code, there is not really complicated things about it.